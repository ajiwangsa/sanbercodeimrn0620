console.log("Soal No. 1 (Array to Object)");
console.log("==========================================");
function arrayToObject(arr) {
  // Code di sini
  if (arr == 0) {
    console.log(" ");
  } else {
    for (i = 0; i < arr.length; i++) {
      var now = new Date();
      var thisYear = now.getFullYear();
      var orang = {};
      var nomor = 1;
      var isi = 0;
      orang.firstname = arr[i][isi++];
      orang.lastname = arr[i][isi++];
      orang.gender = arr[i][isi++];
      if (thisYear > arr[i][isi] || thisYear <= 0) {
        orang.age = thisYear - arr[i][isi];
      } else {
        orang.age = "Invalid Birth Year";
      }
      nomor = nomor + i;
      console.log(nomor + ". " + orang.firstname + " " + orang.lastname + " :");
      console.log(orang);
    }
  }
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]); // ""

console.log("\nSoal No. 2 (Shopping Time)");
console.log("==========================================");
function shoppingTime(memberId, money) {
  // you can only write your code here!
  var product = [
    ["Sepatu Stacattu", 1500000],
    ["Baju Zoro", 500000],
    ["Baju H&N", 250000],
    ["Sweater Uniklooh", 175000],
    ["Casing Handphone", 50000],
  ];
  if (memberId == "" || memberId == undefined) {
    return "Mohon Maaf, Toko X hanya berlaku untuk member saja";
  } else if (money < 50000 || money == undefined) {
    return "Mohon Maaf uang anda tidak cukup";
  } else {
    var uang = money;
    var i = 0;
    var j = 0;
    var array = [];
    while (money >= 50000 && i < product.length) {
      if (money >= product[i][1]) {
        money = money - product[i][1];
        array[j] = product[i][0];
        j++;
      }
      i++;
    }
    var cetak = {
      memberId: memberId,
      money: uang,
      listPurchased: array,
      changeMoney: money,
    };
    return cetak;
  }
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log("\nSoal No. 3 (Naik Angkot)");
console.log("==========================================");
function naikAngkot(arrPenumpang) {
  rute = ["A", "B", "C", "D", "E", "F"];
  //your code here
  if (arrPenumpang == undefined) {
    return;
  } else {
    var bayar = 2000;
    var deret = [];
    for (i = 0; i < arrPenumpang.length; i++) {
      dari = rute.indexOf(arrPenumpang[i][1]);
      tujuan = rute.indexOf(arrPenumpang[i][2]);
      uang = (tujuan - dari) * bayar;
      j = 0;
      deret[i] = {
        penumpang: arrPenumpang[i][j++],
        naikDari: arrPenumpang[i][j++],
        tujuan: arrPenumpang[i][j++],
        bayar: uang,
      };
    }
    return deret;
  }
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
