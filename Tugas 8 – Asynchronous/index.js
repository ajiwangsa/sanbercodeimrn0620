// di index.js
console.log("1. CallBack Baca Buku");
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Tulis code untuk memanggil function readBooks di sini
const read = (i, time) => {
	i > books.length -1 ? '' :
	readBooks(time,books[i],time=> {
		return i + read(i + 1, time)
	})
}
read(0,10000)