import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Noter from "./Tugas/Tugas14/App";
import Skill from "./Tugas/Tugas14/SkillScreen";
export default function App() {
  // return <Noter />;
  return <Skill />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
