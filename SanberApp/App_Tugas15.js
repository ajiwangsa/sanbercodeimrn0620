import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Drawer from "./Tugas/Tugas15/Index";
import TugasNav from "./Tugas/TugasNavigation/index";

export default function App() {
  // return <Drawer />;
  return <TugasNav />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
