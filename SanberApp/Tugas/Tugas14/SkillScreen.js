import React from "react";
import { StyleSheet, Text, View, Image, FlatList } from "react-native";
import { FontAwesome } from "@expo/vector-icons";
import SkillItem from "./components/skillItem";
import skillData from "./skillData.json";

export default class SkillPage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topBar}>
          <Image
            source={require("./images/logo.png")}
            style={{ width: 187, height: 51 }}
          />
        </View>

        <View style={styles.nameBar}>
          <FontAwesome
            style={{ paddingTop: 5 }}
            name="user-circle"
            size={26}
            color="#3EC6FF"
          />
          <View style={styles.helloBar}>
            <Text style={styles.teks1}>Hai,</Text>
            <Text style={styles.teks2}>Awang</Text>
          </View>
        </View>
        <Text style={styles.teks3}>SKILL</Text>

        <View style={styles.garis} />

        <View style={styles.skillBar}>
          <View style={styles.skilldetailBar}>
            <Text style={styles.fontskilldetail}>Library/Framework</Text>
          </View>
          <View style={styles.skilldetailBar}>
            <Text style={styles.fontskilldetail}>Bahasa Pemrograman</Text>
          </View>
          <View style={styles.skilldetailBar}>
            <Text style={styles.fontskilldetail}>Teknologi</Text>
          </View>
        </View>
        <View style={styles.techBar}>
          <FlatList
            data={skillData.items}
            renderItem={(skill) => <SkillItem skill={skill.item} />}
            keyExtractor={(item) => item.id}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  topBar: {
    backgroundColor: "white",
    paddingHorizontal: 15,
    alignItems: "flex-end",
  },
  nameBar: {
    backgroundColor: "white",
    paddingHorizontal: 15,
    flexDirection: "row",
  },
  helloBar: {
    alignContent: "space-around",
    paddingLeft: 10,
  },
  teks1: {
    color: "#000000",
    fontSize: 12,
  },
  teks2: {
    color: "#003366",
    fontSize: 16,
  },
  teks3: {
    color: "#003366",
    fontSize: 36,
    paddingLeft: 15,
  },
  garis: {
    height: 4,
    backgroundColor: "#3EC6FF",
    width: 343,
    alignSelf: "center",
  },
  skillBar: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingTop: 10,
  },
  skilldetailBar: {
    height: 32,
    backgroundColor: "#B4E9FF",
    justifyContent: "center",
    borderRadius: 8,
  },
  fontskilldetail: {
    color: "#003366",
    fontSize: 12,
    fontWeight: "bold",
  },
  techBar: {
    flexDirection: "column",
    paddingTop: 10,
  },
});
