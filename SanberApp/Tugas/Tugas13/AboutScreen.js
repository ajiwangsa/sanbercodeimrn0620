import React, { Component } from "react";
import {
  TextInput,
  Platform,
  Image,
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
export default class About extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.regView}>
          <Text style={styles.regText}>Tentang Saya</Text>
        </View>
        <View style={styles.logo}>
          <Image
            source={require("./images/user.png")}
            style={{ width: 90, height: 90 }}
          />
        </View>
        <View style={styles.namaView}>
          <Text style={styles.namaText}>AJI AWANG SETIAWAN</Text>
        </View>
        <View style={styles.jbtView}>
          <Text style={styles.jbtText}>React Native Developer</Text>
        </View>

        <View style={styles.optionContainer}>
          <Text strokeWidth="1"> Portofolio</Text>
          <View style={styles.gambar1}>
            <Image
              style={styles.gambar1}
              source={require("./images/gitlab.png")}
              style={{ width: 50, height: 50 }}
            />
            <Text>@ajiwangsa</Text>
          </View>
          <View style={styles.gambar2}>
            <Image
              source={require("./images/github.png")}
              style={{ width: 50, height: 50 }}
            />
            <Text>@ajiwangsa</Text>
          </View>
        </View>

        <View style={styles.optionContainer1}>
          <View>
            <Text> Hubungi Saya</Text>
          </View>
          <View style={styles.gambar3}>
            <Image
              source={require("./images/facebook.png")}
              style={{ width: 50, height: 50 }}
            />
            <Text style={styles.text1}> awang.aji.setiawan</Text>
            <Image
              source={require("./images/ig.png")}
              style={{ width: 50, height: 50 }}
            />
            <Text style={styles.text2}> @ajiwangsa</Text>
            <Image
              source={require("./images/twit.png")}
              style={{ width: 50, height: 50 }}
            />
            <Text style={styles.text3}> @ajiwangsa</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  text1: {
    position: "absolute",
    left: "20.03%",
    right: "3.22%",
    top: "10.99%",
    bottom: "6.51%",
  },
  text2: {
    position: "absolute",
    left: "20.03%",
    right: "3.22%",
    top: "43.99%",
    bottom: "6.51%",
  },
  text3: {
    position: "absolute",
    left: "20.03%",
    right: "3.22%",
    top: "76.99%",
    bottom: "6.51%",
  },
  gambar1: {
    position: "absolute",
    left: "5%",
    right: "3.22%",
    top: "20.99%",
    bottom: "6.51%",
  },
  gambar2: {
    position: "absolute",
    left: "70%",
    right: "0.25%",
    top: "20.25%",
    bottom: "20.25%",
  },
  gambar3: {
    position: "absolute",
    left: "20.03%",
    right: "3.22%",
    top: "20.99%",
    bottom: "6.51%",
  },
  optionContainer: {
    flexDirection: "row",
    flexWrap: "wrap",
    width: "80%",
    backgroundColor: "#EFEFEF",
    borderRadius: 10,
    height: 110,
    marginBottom: 20,
    color: "#003366",
  },
  optionContainer1: {
    flexDirection: "row",
    flexWrap: "wrap",
    width: "80%",
    backgroundColor: "#EFEFEF",
    borderRadius: 10,
    height: 200,
    marginBottom: 20,
    color: "#003366",
  },
  //   borderr: {
  //     borderColor: "black",
  //     borderWidth: 1,
  //   },
  //   inputView: {
  //     backgroundColor: "#EFEFEF",
  //     height: 100,
  //     justifyContent: "center",
  //     padding: 20,
  //   },
  namaView: {
    fontWeight: "bold",
    fontSize: 12,
    color: "#003366",
    marginBottom: 5,
  },
  namaText: {
    color: "#003366",
    fontSize: 24,
  },
  jbtView: {
    fontWeight: "bold",
    fontSize: 12,
    color: "#3EC6FF",
    marginBottom: 20,
  },
  jbtText: {
    color: "#3EC6FF",
    fontSize: 18,
  },
  //   regview: {
  //     marginTop: 20,
  //     marginBottom: 50,
  //   },
  regText: {
    marginBottom: 20,
    color: "#003366",
    fontSize: 35,
  },
});
