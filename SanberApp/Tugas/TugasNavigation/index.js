import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { Button } from "react-native";

import LoginScreen from "./LoginScreen";
import AboutScreen from "./AboutScreen";
import SkillScreen from "./SkillScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";

import { SimpleLineIcons } from "@expo/vector-icons";

const Tab = createBottomTabNavigator();
const TabScreen = () => (
  <Tab.Navigator>
    <Tab.Screen name="Skill" component={SkillScreen} />
    <Tab.Screen name="Project" component={ProjectScreen} />
    <Tab.Screen name="Add" component={AddScreen} />
  </Tab.Navigator>
);

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={TabScreen} />
    <Drawer.Screen name="About" component={AboutScreen} />
  </Drawer.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator>
    <RootStack.Screen name="Login" component={LoginScreen} />
    <RootStack.Screen
      name="Welcome"
      component={DrawerScreen}
      options={({ navigation }) => ({
        headerLeft: () => (
          <SimpleLineIcons
            name="logout"
            size={30}
            color="black"
            style={{ marginLeft: 20 }}
            onPress={() => navigation.navigate("Login")}
          />
        ),
      })}
    />
  </RootStack.Navigator>
);

export default () => (
  <NavigationContainer>
    <RootStackScreen />
    {/*<DrawerScreen />*/}
  </NavigationContainer>
);
