import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { FontAwesome, Ionicons } from "@expo/vector-icons";

export default class AboutPage extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topBar}>
          <Text style={styles.textPage}>Tentang Saya</Text>
          <FontAwesome
            style={styles.iconItem}
            name="user-circle"
            size={200}
            color="#EFEFEF"
          />
          <Text
            style={{
              color: "#003366",
              fontSize: 22,
              fontWeight: "bold",
            }}
          >
            Aji Awang Setiawan
          </Text>
          <Text
            style={{
              color: "#3EC6FF",
              fontSize: 15,
              fontWeight: "bold",
            }}
          >
            React Native Developer
          </Text>
        </View>
        <View style={styles.middleBar}>
          <Text
            style={{
              color: "#003366",
              fontSize: 18,
              marginTop: 3,
              marginLeft: 10,
            }}
          >
            Portofolio
          </Text>
          <View
            style={{
              height: 1,
              backgroundColor: "#003366",
              width: 343,
              alignSelf: "center",
            }}
          />
          <View style={styles.portoBar}>
            <TouchableOpacity style={{ padding: 10, alignItems: "center" }}>
              <FontAwesome name="gitlab" size={42} color="#3EC6FF" />
              <Text
                style={{
                  color: "#003366",
                  fontSize: 15,
                  fontWeight: "bold",
                }}
              >
                @ajiwangsa
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ padding: 10, alignItems: "center" }}>
              <Ionicons name="logo-github" size={42} color="#3EC6FF" />
              <Text
                style={{
                  color: "#003366",
                  fontSize: 15,
                  fontWeight: "bold",
                }}
              >
                @ajiwangsa
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.lowerBar}>
          <Text
            style={{
              color: "#003366",
              fontSize: 18,
              marginTop: 3,
              marginLeft: 10,
            }}
          >
            Hubungi Saya
          </Text>
          <View
            style={{
              height: 1,
              backgroundColor: "#003366",
              width: 343,
              alignSelf: "center",
            }}
          />

          <View style={styles.medsosBar}>
            <TouchableOpacity style={{ padding: 5, alignItems: "center" }}>
              <FontAwesome name="facebook-official" size={39} color="#3EC6FF" />
              <Text
                style={{
                  color: "#003366",
                  fontSize: 16,
                  fontWeight: "bold",
                }}
              >
                awang.aji.setiawan
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ padding: 5, alignItems: "center" }}>
              <FontAwesome name="instagram" size={39} color="#3EC6FF" />
              <Text
                style={{
                  color: "#003366",
                  fontSize: 15,
                  fontWeight: "bold",
                }}
              >
                ajiwangsa
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ padding: 5, alignItems: "center" }}>
              <FontAwesome name="telegram" size={39} color="#3EC6FF" />
              <Text
                style={{
                  color: "#003366",
                  fontSize: 16,
                  fontWeight: "bold",
                }}
              >
                @ajiwangsa
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
  },
  topBar: {
    backgroundColor: "white",
    alignItems: "center",
  },
  textPage: {
    color: "#003366",
    fontSize: 36,
    fontWeight: "bold",
  },
  iconItem: {
    marginTop: 15,
  },
  middleBar: {
    backgroundColor: "#EFEFEF",
    width: 359,
    height: 140,
    borderRadius: 16,
    marginTop: 10,
  },
  portoBar: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  lowerBar: {
    backgroundColor: "#EFEFEF",
    width: 359,
    height: 251,
    borderRadius: 16,
    marginTop: 10,
  },
  medsosBar: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
});
