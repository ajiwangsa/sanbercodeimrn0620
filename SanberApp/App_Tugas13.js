import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Register from "./Tugas/Tugas13/RegisterScreen";
import About from "./Tugas/Tugas13/AboutScreen";
import Login from "./Tugas/Tugas13/LoginScreen";
export default function App() {
  return <Login />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
