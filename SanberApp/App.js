import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
// import Drawer from "./Tugas/Tugas15/Index";
import Quiz3 from "./Quiz3/index";

export default function App() {
  // return <Drawer />;
  return <Quiz3 />;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
