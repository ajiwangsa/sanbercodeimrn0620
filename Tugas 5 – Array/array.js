// Soal No. 1 (Range)
    function range(startNum, finishNum) {
        nomor=[]
        if (!finishNum || !startNum) {
            return -1
        }
        else {
            if (startNum>finishNum) {
                for (i=startNum;i>=finishNum;i--) {
                    nomor.push(i) 
                }
            }
            else {
                for (i=startNum;i<=finishNum;i++) {
                    nomor.push(i) 
                }
            }
        }
        return nomor
    }
    console.log(range(1,10)) ; 
    console.log(range(1));
    console.log(range(11,18));
    console.log(range(54, 50));
    console.log(range()) ;

// Soal No. 2 (Range with Step)
    function rangeWithStep(startNum=0, finishNum=0, step=1) {    
        nomor=[]
        if (startNum>finishNum) {
            for (i=startNum;i>=finishNum;i-=step) {
                nomor.push(i) 
            }
        }
        else {
            for (i=startNum;i<=finishNum;i+=step) {
                nomor.push(i) 
            }
        }
        return nomor
    }
    console.log(rangeWithStep(1, 10, 2)) ;
    console.log(rangeWithStep(11, 23, 3)) ;
    console.log(rangeWithStep(5, 2, 1)) ;
    console.log(rangeWithStep(29, 2, 4)) ;

// Soal No. 3 (Sum of Range)
    function sum(satu, dua, tiga) {   
        jumlah=0
        panggil = rangeWithStep(satu,dua,tiga)
        panjang = panggil.length
        for (i = 0; i < panjang; i++) {
            jumlah += panggil[i]
        }
        return jumlah
    }
    console.log(sum(1,10)) 
    console.log(sum(5, 50, 2)) 
    console.log(sum(15,10)) 
    console.log(sum(20, 10, 2)) 
    console.log(sum(1)) 
    console.log(sum())  

// Soal No. 4 (Array Multidimensi)
    function dataHandling(input){
        for(i=0;i<input.length;i++){
            var j = 0
            console.log("Nomor ID : "+input[i][j++])
            console.log("Nama Lengkap : "+input[i][j++])
            console.log("TTL : "+input[i][j++]+" "+input[i][j++])
            console.log("Hobi : "+input[i][j++]+"\n")
        }
    }
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ] 
    dataHandling(input);

// Soal No. 5 (Balik Kata)
    function balikKata(kata){
        kebalik=''
        for(i = kata.length-1;i>=0;i--){
            kebalik = kebalik+kata.charAt(i)
        }
        return kebalik
    }

    console.log(balikKata("Kasur Rusak"));
    console.log(balikKata("SanberCode"));
    console.log(balikKata("Haji Ijah"));
    console.log(balikKata("racecar"));
    console.log(balikKata("I am Sanbers"));

    // Soal No. 6 (Metode Array)
    function dataHandling2(input) {
        input.splice(1,1,"Roman Alamsyah Elsharawy")
        input.splice(2,1,"Provinsi Bandar Lampung")
        input.splice(4,1,"Pria")
        input.splice(5,0,"SMA International Metro")
        console.log(input);
        
        var tgl=input[3]
        var tanggal = tgl.split("/")
        switch(tanggal[1]){
        case "01": {console.log("Januari");break;}
        case "02": {console.log("Februari");break;}
        case "03": {console.log("Maret");break;}
        case "04": {console.log("April");break;}
        case "05": {console.log("Mei");break;}
        case "06": {console.log("Juni");break;}
        case "07": {console.log("Juli");break;}
        case "08": {console.log("=Agustus");break;}
        case "09": {console.log("September");break;}
        case "10": {console.log("Oktober");break;}
        case "11": {console.log("November");break;}
        case "12": {console.log("Desember");break;}
        default: {console.log("Tidak valid");break;}
        }
        var tanggal1=tanggal.join("-")
        console.log( tanggal.sort(function(a, b){return b-a}));
        console.log(tanggal1);
    
        var nama=input[1]
        console.log(nama.slice(0,14));
    }
    var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
    dataHandling2(input)